from litestar import LiteStarApp, LitestarResponse
from litestar.routing import Route

app = LiteStarApp()

async def proof_item_detail(request):
    proof_item_id = int(request.path_params['proof_item_id'])
    # Simulate fetching item details
    item = get_cart_line_item_helper(proof_item_id)
    return LitestarResponse(render_template('proof_item_detail.html', item=item, proof_item_id=proof_item_id))

def get_cart_line_item_helper(proof_item_id):
    # Dummy implementation of the helper function
    return {'Scene7Url': 'https://example.com/scene7image.jpg'}

routes = [
    Route('/proof_item/{proof_item_id}', proof_item_detail),
]

app.routes.extend(routes)

if __name__ == "__main__":
    app.run('127.0.0.1', 8000)

