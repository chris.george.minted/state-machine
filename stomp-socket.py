"""
REPLACE ME WITH CELERY OR KAFKA
BUT THS IS A GOOD START
"""

import time
import stomp


class MyListener(stomp.ConnectionListener):
    def on_error(self, headers, message):
        print("received an error %s" % message)

    def on_message(self, headers, message):
        print("received a message %s" % message)
        if message == "test":
            print("Worked")
            self.conn.ack(headers["message-id"], headers["subscription"])
        elif message == "quite":
            print("Done")
            self.conn.ack(headers["message-id"], headers["subscription"])
            self.conn.disconnect()
            return
        else:
            print("Failed")


conn = stomp.Connection([("localhost", 61613)])
conn.set_listener("", MyListener())
conn.connect(wait=True)
conn.subscribe(destination="/queue/test", id=1, ack="auto")

while conn.is_connected():
    print("waiting")
    time.sleep(5)
