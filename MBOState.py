class MBOState:
    def __init__(self):
        self.name = None
        self.id = None
        self.isTerminal = None
        self.isInitial = None
        self.diagramName = None
        self.diagramId = None
        self.label = None
class MBOState:
    def __init__(self):
        self.name = None
        self.id = None
        self.isTerminal = None
        self.isInitial = None
        self.diagramName = None
        self.diagramId = None
        self.label = None


import stomp

class MBOStateDiagram:
    ORDER_DIAGRAM = "order"
    PROOF_DIAGRAM = "proof"
    RANDRPO_DIAGRAM = "randr po"
    BRANDPO_DIAGRAM = "brand po"
    SAMPLEPO_DIAGRAM = "sample po"
    PROOF_LINE_ITEM_COMPONENT_DIAGRAM = "proof line item component"

    ALL_DIAGRAMS = [
        ORDER_DIAGRAM,
        RANDRPO_DIAGRAM,
        PROOF_DIAGRAM,
        BRANDPO_DIAGRAM,
        SAMPLEPO_DIAGRAM,
        PROOF_LINE_ITEM_COMPONENT_DIAGRAM
    ]

    DIAGRAM_NAME_TO_ID_MAP = {
        ORDER_DIAGRAM: 1,
        PROOF_DIAGRAM: 2,
        RANDRPO_DIAGRAM: 3,
        BRANDPO_DIAGRAM: 4,
        SAMPLEPO_DIAGRAM: 5,
        PROOF_LINE_ITEM_COMPONENT_DIAGRAM: 6
    }

    def __init__(self):
        self._stateNameToMBOStateMap = {}
        self._processorToTransitionArrayMap = {}
        self.mboStateDiagramInstance = None

    @staticmethod
    def instance():
        if MBOStateDiagram.mboStateDiagramInstance is None:
            MBOStateDiagram.mboStateDiagramInstance = MBOStateDiagram()
        return MBOStateDiagram.mboStateDiagramInstance

    def getAllStatesByDiagramName(self, name):
        rv = []
        allMBOStates = list(self._stateNameToMBOStateMap.values())
        for MBOState in allMBOStates:
            if name == MBOState.diagramName:
                rv.append(MBOState)
        return rv

    # ... rest of the methods

