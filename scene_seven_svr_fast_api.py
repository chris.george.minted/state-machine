from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

app = FastAPI()

templates = Jinja2Templates(directory="templates")

@app.get('/proof_item/{proof_item_id}')
async def proof_item_detail(request: Request, proof_item_id: int):
    # Simulate fetching item details
    item = get_cart_line_item_helper(proof_item_id)
    if item is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return templates.TemplateResponse("proof_item_detail.html", {"request": request, "item": item, "proof_item_id": proof_item_id})

def get_cart_line_item_helper(proof_item_id):
    # Dummy implementation of the helper function
    return {'Scene7Url': 'https://example.com/scene7image.jpg'}

