from celery_config import app
from tasks import send_message_to_queue, forced_transit, transit
from enum import Enum
import stomp
import logging

# Send a message to queue
send_message_to_queue.delay("state_name", entity_id, props={"key": "value"})

# Perform a forced transition
forced_transit.delay(
    entity_id,
    "from_state_name",
    "to_state_name",
    "result_code",
    "result_desc",
    "account_id",
)

# Perform a state transition
transit.delay(
    entity_id,
    {
        "from_state_name": "ORDER_DIAGRAM",
        "trans_result_code": "RESULT_CODE_1",
        "trans_result_description": "Transition successful",
        "account_id": 12345,
    },
)
# Setting up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)



class DiagramType(Enum):
    ORDER_DIAGRAM = 1
    RANDRPO_DIAGRAM = 3
    PROOF_DIAGRAM = 2
    PROOF_LINE_ITEM_COMPONENT_DIAGRAM = 6


diagram_mapping = {
    DiagramType.ORDER_DIAGRAM: MintedOrder,
    DiagramType.RANDRPO_DIAGRAM: MintedPo,
    DiagramType.PROOF_DIAGRAM: MintedProof,
    DiagramType.PROOF_LINE_ITEM_COMPONENT_DIAGRAM: MintedProofLineItemComponentAsset
}

class MintedStateTransitionLog:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def save(self):
        # Implement the save logic here
        pass


class MintedOrder:
    @staticmethod
    def load_by_id(entity_id):
        # Implement the load logic here
        return MintedOrder()

    def save(self):
        # Implement the save logic here
        pass


class MintedPo:
    @staticmethod
    def load_by_id(entity_id):
        # Implement the load logic here
        return MintedPo()

    def save(self):
        # Implement the save logic here
        pass


class MintedProof:
    @staticmethod
    def load_by_id(entity_id):
        # Implement the load logic here
        return MintedProof()

    def save(self):
        # Implement the save logic here
        pass


class MintedProofLineItemComponentAsset:
    @staticmethod
    def load_by_id(entity_id):
        # Implement the load logic here
        return MintedProofLineItemComponentAsset()

    def save(self):
        # Implement the save logic here
        pass


class VisitorService:
    @staticmethod
    def instance():
        return VisitorService()

    def get_current_visitor(self):
        return Visitor()


class Visitor:
    def get_account_id(self):
        return 12345


class MintedLogger:
    @staticmethod
    def info(message):
        logger.info(message)

    @staticmethod
    def warn(message):
        logger.warning(message)

    @staticmethod
    def exception(exception):
        logger.exception(exception)

@app.task
def send_message_to_queue(state_name, entity_id, props=None):
    con = stomp.Connection([("localhost", 61613)])
    con.connect(wait=True)

    headers = {"persistent": "true"}
    if props:
        headers.update(props)
    queue_name = f"/queue/{state_name}"
    con.send(destination=queue_name, body=str(entity_id), headers=headers)
    con.disconnect()


@app.task
def forced_transit(
    entity_id, from_state_name, to_state_name, result_code, result_desc, account_id
):
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(from_state_name),
        ToMintedStateDiagramStateId=get_state_id_from_state_name(to_state_name),
        MintedTransitionResultCode=result_code,
        TransitionResult=result_desc,
        MintedAccountId=account_id,
    )

    if update_entity_state(
        entity_id,
        trans_log.ToMintedStateDiagramStateId,
        trans_log.FromMintedStateDiagramStateId,
    ):
        trans_log.save()

    MintedLogger.info(
        f"forcedTransit entity [{entity_id}] from [{from_state_name}] to [{to_state_name}] result [{result_code}] account [{account_id}]"
    )


@app.task
def transit(entity_id, trans_result):
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(
            trans_result["from_state_name"]
        ),
    )

    for trans in get_transitions():
        if (
            trans["from_state"] == trans_result["from_state_name"]
            and trans["trans_result_code"] == trans_result["trans_result_code"]
        ):
            trans_log.ToMintedStateDiagramStateId = get_state_id_from_state_name(
                trans["to_state"]
            )
            to_state_name = trans["to_state"]
            break

    if "to_state_name" not in locals():
        raise Exception(
            f"Cannot find to state [{trans_result['from_state_name']}] [{trans_result['trans_result_code']}]"
        )

    MintedLogger.info(
        f"transit [{entity_id}] [{trans_result['from_state_name']}] [{to_state_name}] [{trans_result['trans_result_code']}] [{trans_result['trans_result_description']}]"
    )

    trans_log.MintedAccountId = (
        trans_result.get("account_id")
        or VisitorService.instance().get_current_visitor().get_account_id()
    )
    trans_log.save()


def get_state_id_from_state_name(state_name):
    # Dummy implementation, replace with actual logic
    state_name_to_id = {
        "ORDER_DIAGRAM": 1,
        "PROOF_DIAGRAM": 2,
        "RANDRPO_DIAGRAM": 3,
        "BRANDPO_DIAGRAM": 4,
        "SAMPLEPO_DIAGRAM": 5,
        "PROOF_LINE_ITEM_COMPONENT_DIAGRAM": 6,
    }
    return state_name_to_id.get(state_name, -1)


def update_entity_state(entity_id, state_id, from_state_id):
    if state_id == 1:  # Assuming 1 corresponds to ORDER_DIAGRAM
        order = MintedOrder.load_by_id(entity_id)
        order.MintedStateDiagramStateId = state_id
        order.save()
        return True
    elif state_id == 3:  # Assuming 3 corresponds to RANDRPO_DIAGRAM
        po = MintedPo.load_by_id(entity_id)
        po.MintedStateDiagramStateId = state_id
        po.save()
        return True
    elif state_id == 2:  # Assuming 2 corresponds to PROOF_DIAGRAM
        proof = MintedProof.load_by_id(entity_id)
        proof.MintedStateDiagramStateId = state_id
        proof.save()
        return True
    elif state_id == 6:  # Assuming 6 corresponds to PROOF_LINE_ITEM_COMPONENT_DIAGRAM
        component_asset = MintedProofLineItemComponentAsset.load_by_id(entity_id)
        component_asset.MintedStateDiagramStateId = state_id
        component_asset.save()
        return True
    else:
        raise Exception(f"{state_id} not implemented")

# alternative implementation
def update_entity_state_via_dag(entity_id, state_id, from_state_id):
    """_summary_

    Args:
        entity_id (_type_): _description_
        state_id (_type_): _description_
        from_state_id (_type_): _description_

    Raises:
        Exception: _description_

    Returns:
        _type_: _description_
    """    
    # Assuming 'state_id' is the state ID to check against the Enum
    selected_diagram_type = None
    for diagram_type in DiagramType:
        if diagram_type.value == state_id:
            selected_diagram_type = diagram_type
            break

    if selected_diagram_type is None:
        raise Exception(f"{state_id} not implemented")

    # Load the appropriate object based on selected diagram type
    diagram_class = diagram_mapping[selected_diagram_type]
    diagram_object = diagram_class.load_by_id(entity_id)
    diagram_object.MintedStateDiagramStateId = state_id
    diagram_object.save()

    return True


def get_transitions():
    # Dummy implementation, replace with actual logic
    return [
        {
            "from_state": "ORDER_DIAGRAM",
            "to_state": "PROOF_DIAGRAM",
            "trans_result_code": "RESULT_CODE_1",
            "worker": "ProcessorName1",
        },
        {
            "from_state": "RANDRPO_DIAGRAM",
            "to_state": "SAMPLEPO_DIAGRAM",
            "trans_result_code": "RESULT_CODE_2",
            "worker": "ProcessorName2",
        },
    ]


from celery import Celery

app = Celery(
    "tasks", broker="redis://localhost:6379/0", backend="redis://localhost:6379/0"
)

app.conf.update(
    result_expires=3600,
    task_routes={
        "tasks.send_message_to_queue": {"queue": "messages"},
        "tasks.forced_transit": {"queue": "transitions"},
        "tasks.transit": {"queue": "transitions"},
    },
)

from celery_config import app
import stomp
import logging

# Setting up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Implement any required classes here, such as MintedStateTransitionLog, MintedLogger, etc.


@app.task
def send_message_to_queue(state_name, entity_id, props=None):
    con = stomp.Connection([("localhost", 61613)])
    con.connect(wait=True)

    headers = {"persistent": "true"}
    if props:
        headers.update(props)
    queue_name = f"/queue/{state_name}"
    con.send(destination=queue_name, body=str(entity_id), headers=headers)
    con.disconnect()


@app.task
def forced_transit(
    entity_id, from_state_name, to_state_name, result_code, result_desc, account_id
):
    # Implement the logic here
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(from_state_name),
        ToMintedStateDiagramStateId=get_state_id_from_state_name(to_state_name),
        MintedTransitionResultCode=result_code,
        TransitionResult=result_desc,
        MintedAccountId=account_id,
    )

    if update_entity_state(
        entity_id,
        trans_log.ToMintedStateDiagramStateId,
        trans_log.FromMintedStateDiagramStateId,
    ):
        trans_log.save()

    logger.info(
        f"forcedTransit entity [{entity_id}] from [{from_state_name}] to [{to_state_name}] result [{result_code}] account [{account_id}]"
    )


@app.task
def transit(entity_id, trans_result):
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(
            trans_result.from_state_name
        ),
    )

    for trans in get_transitions():
        if (
            trans.from_state == trans_result.from_state_name
            and trans.trans_result_code == trans_result.trans_result_code
        ):
            trans_log.ToMintedStateDiagramStateId = get_state_id_from_state_name(
                trans.to_state
            )
            to_state_name = trans.to_state
            break

    if "to_state_name" not in locals():
        raise Exception(
            f"Cannot find to state [{trans_result.from_state_name}] [{trans_result.trans_result_code}]"
        )

    logger.info(
        f"transit [{entity_id}] [{trans_result.from_state_name}] [{to_state_name}] [{trans_result.trans_result_code}] [{trans_result.trans_result_description}]"
    )

    trans_log.MintedAccountId = (
        trans_result.account_id
        or VisitorService.instance().get_current_visitor().get_account_id()
    )
    trans_log.save()


# Implement any required utility functions here, such as get_state_id_from_state_name, update_entity_state, etc.


from celery_config import app
import stomp
import logging

# Setting up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Implement any required classes here, such as MintedStateTransitionLog, MintedLogger, etc.


@app.task
def send_message_to_queue(state_name, entity_id, props=None):
    con = stomp.Connection([("localhost", 61613)])
    con.connect(wait=True)

    headers = {"persistent": "true"}
    if props:
        headers.update(props)
    queue_name = f"/queue/{state_name}"
    con.send(destination=queue_name, body=str(entity_id), headers=headers)
    con.disconnect()


@app.task
def forced_transit(
    entity_id, from_state_name, to_state_name, result_code, result_desc, account_id
):
    # Implement the logic here
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(from_state_name),
        ToMintedStateDiagramStateId=get_state_id_from_state_name(to_state_name),
        MintedTransitionResultCode=result_code,
        TransitionResult=result_desc,
        MintedAccountId=account_id,
    )

    if update_entity_state(
        entity_id,
        trans_log.ToMintedStateDiagramStateId,
        trans_log.FromMintedStateDiagramStateId,
    ):
        trans_log.save()

    logger.info(
        f"forcedTransit entity [{entity_id}] from [{from_state_name}] to [{to_state_name}] result [{result_code}] account [{account_id}]"
    )


@app.task
def transit(entity_id, trans_result):
    trans_log = MintedStateTransitionLog(
        EntityId=entity_id,
        FromMintedStateDiagramStateId=get_state_id_from_state_name(
            trans_result.from_state_name
        ),
    )

    for trans in get_transitions():
        if (
            trans.from_state == trans_result.from_state_name
            and trans.trans_result_code == trans_result.trans_result_code
        ):
            trans_log.ToMintedStateDiagramStateId = get_state_id_from_state_name(
                trans.to_state
            )
            to_state_name = trans.to_state
            break

    if "to_state_name" not in locals():
        raise Exception(
            f"Cannot find to state [{trans_result.from_state_name}] [{trans_result.trans_result_code}]"
        )

    logger.info(
        f"transit [{entity_id}] [{trans_result.from_state_name}] [{to_state_name}] [{trans_result.trans_result_code}] [{trans_result.trans_result_description}]"
    )

    trans_log.MintedAccountId = (
        trans_result.account_id
        or VisitorService.instance().get_current_visitor().get_account_id()
    )
    trans_log.save()


# Implement any required utility functions here, such as get_state_id_from_state_name, update_entity_state, etc.
